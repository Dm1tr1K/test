import { ImgHTMLAttributes } from "react";

type LoaderSizes = "min" | "large";
type LoaderColor = "primary" | "danger" | "warning" | "success" | "default";

export interface ILoaderProps extends ImgHTMLAttributes<HTMLImageElement> {
  loaderSize: LoaderSizes;
  loaderColor: LoaderColor;
}
