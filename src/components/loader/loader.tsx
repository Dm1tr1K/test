import style from "./loader.module.css";
import { ILoaderProps } from "./loader.types";
import cn from "classnames";

const Loader: React.FC<ILoaderProps> = ({ loaderSize, loaderColor, ...props }) => {
  const loaderStyles = cn({
    [style["loader"]]: true,
    [style[loaderSize]]: true,
    [style[loaderColor]]: true,
  });

  return <div className={loaderStyles}></div>;
};

export default Loader;
