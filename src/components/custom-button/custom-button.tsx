import { IButtonProps } from "./custom-button.types";
import style from "./custom-button.module.css";
import Loader from "../loader/loader";
import cn from "classnames";

const CustomButton: React.FC<IButtonProps> = ({
  loading,
  onClick,
  buttonType = "primary",
  buttonSize,
  block,
  buttonMod = "default",
  children,
  disabled = false,
  ...props
}) => {
  const buttonStyles = cn({
    [style["btn"]]: true,
    [style[buttonType]]: true,
    [style[buttonSize]]: true,
    [style.block]: block,
    [style[buttonMod]]: buttonMod == "default" ? false : true,
  });

  return (
    <button
      className={buttonStyles}
      onClick={() => onClick()}
      disabled={loading ? loading : disabled}
    >
      <span className={style.btnContent}>
        {loading ? (
          <Loader
            loaderSize={buttonSize}
            loaderColor={buttonMod === "default" ? buttonMod : buttonType}
          />
        ) : (
          children
        )}
      </span>
    </button>
  );
};

export default CustomButton;
