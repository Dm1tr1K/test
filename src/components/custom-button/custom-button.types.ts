import { ButtonHTMLAttributes } from "react";

type ButtonType = "danger" | "success" | "warning" | "primary";
type ButtonSize = "min" | "large";
type ButtonMod = "default" | "outlined" | "text";

export interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  loading?: boolean;
  onClick: () => void;
  buttonType?: ButtonType;
  buttonSize: ButtonSize;
  block?: boolean;
  buttonMod?: ButtonMod;
  disabled?: boolean;
  children?: string | React.ReactNode;
}
