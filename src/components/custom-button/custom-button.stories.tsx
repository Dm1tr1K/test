import CustomButton from "./custom-button";
import AppleIcon from "../../assets/icons/apple.svg";

export default {
  title: "Custom Button",
  component: CustomButton,
  tags: ["autodocs"],
  argTypes: {
    buttonType: {
      control: {
        type: "radio",
      },
      options: ["primary", "danger", "success", "warning"],
      description: "Тип кнопки",
    },
    buttonSize: {
      control: {
        type: "radio",
      },
      options: ["min", "large"],
      description: "Размер кнопки",
    },
    buttonMod: {
      control: {
        type: "radio",
      },
      options: ["default", "outlined", "text"],
      description: "Стиль кнопки",
    },
    block: {
      description: "Модификатор для растягивания на всю ширину",
    },
    onClick: {
      description: "Обработчик события нажатия на кнопку",
    },
    loading: {
      description: "Отвечает за появление индикатора загрузки внутри кнопки",
    },
    disabled: {
      description: "Блокирует доступ к кнопке и её изменение",
    },
    children: {
      control: {
        type: "radio",
      },
      options: {
        Text: "Text",
        SVG: <img src={AppleIcon} />,
      },
      description: "Получает доступ к дочерним элементам компонента",
    },
  },
};

export const Button = {
  args: {
    loading: true,
    onClick: (): void => alert("Press"),
    buttonType: "primary",
    buttonSize: "large",
    block: false,
    buttonMod: "default",
    disabled: false,
    children: "Text",
  },
};
