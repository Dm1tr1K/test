import style from "./App.module.css";
import CustomButton from "./components/custom-button/custom-button";
import AppleIcon from "./assets/icons/apple.svg";

const onClick = (): void => alert("Меня нажали");

function App() {
  return (
    <div className={style.container}>
      <CustomButton
        onClick={onClick}
        loading={false}
        disabled={false}
        buttonType="danger"
        buttonSize="large"
        block={false}
      >
        <img src={AppleIcon} alt="" />
      </CustomButton>
    </div>
  );
}

export default App;
